'''@file        lab2.py
@brief          Implements LED patterns for a Nucleo L476RG
@details        Implements three LED patterns (square wave, sinewave, and 
                sawtooth) using a FSM with non-blocking code. Sequences are 
                iterated through using the B1 button on the board.
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab2/lab2.py,
                 video at https://photos.app.goo.gl/3jEf5iU768vxBggF8)
@author         Mark Iknoian. 
@date           2/4/21
@image          html lab2_fsm.png "Program FSM"
'''

import utime
import pyb
import math

# Call back function triggered with button input B1
def onButtonPressFCN(IRQ_src):
    '''
    @brief      This function responds to a call back from button B1
    @param      The falling edge of a B1 button press triggers this call back function,
                telling the FSM to advance to the next pattern
    '''
    # Advance_flag is checked each loop for a button input
    # On a state transition, the flag is again set false
    global advance_flag
    advance_flag = 1

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
pinC13 = pyb.Pin (pyb.Pin.cpu.C13) # define variable for user button pin
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN) # When fall edge is detected on PC13, trigger callback fnc
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) # define variable for LED pin with push-pull output 
tim2 = pyb.Timer(2, freq = 20000) # Use timer 2 to create a clock frq of 20khz
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5) # Create channel 1 and set it to PWM mode for pin A5
state = 0 # Initial state is the init state
advance_flag = 0 # Initial state of advance flag
start = utime.ticks_ms() # Total run time counter

if __name__ == "__main__":
    print('''Welcome to the future of interactive entertainment.
To cycle between LED patterns press the blue button
on the Nucleo Entertainment Device. To Start the 
entertainment experience press the blue button. 
Buckle up and enjoy the ride.''')

   
    while True:
        try:
            # main program loop for FSM
            if state==0:
                if advance_flag == 1: # "onbuttonpressFCN" sets flag to advance to next state 
                    print('Sqaure Wave Pattern Selected') # text referencing change printed
                    state = 1 # set state for next loop
                    advance_flag = 0 # reset advance flag for next button press
                    time_s1_start = utime.ticks_ms() # time stamp for next state initialization                                  
        
            elif state==1:
                time_now = utime.ticks_ms() # current up time 
                elapsed = utime.ticks_diff(time_now,time_s1_start)/1000 # calc how long state has been running in sec
                duty = 100*(elapsed % 1) #duty cycle in sec that resets every 1 sec by returning remainder of elapsed time 
                if duty >= 50: # square wave on for duty cycle greater than 50
                    t2ch1.pulse_width_percent(100) # max pulse width/LED brightness
                if duty < 50: # square wave off for duty cycle less than 50
                    t2ch1.pulse_width_percent(0)
                if advance_flag == 1:
                    print('Sinewave Pattern Selected')
                    state = 2
                    advance_flag = 0
                    time_s2_start = utime.ticks_ms()
                
            elif state==2:
                time_now = utime.ticks_ms()
                elapsed = utime.ticks_diff(time_now,time_s2_start)/1000 
                duty = (elapsed % 10) # duty cycle in sec that resets every 10 sec
                pattern_2 = (math.sin(duty*360/10*(math.pi/180))+1)*(100/2) 
                # duty -> range from 0-360 every 10 sec -> radians -> 
                # sin output (with +1 for start position) -> 
                # half value * 100 for LED 0-100 PWM percentage
                t2ch1.pulse_width_percent(pattern_2) # use generated % for LED brightness
                if advance_flag == 1:
                    print('Sawtooth Pattern Selected')
                    state = 3
                    advance_flag = 0
                    time_s3_start = utime.ticks_ms()
                
            elif state==3:
                time_now = utime.ticks_ms()
                elapsed = utime.ticks_diff(time_now,time_s3_start)/1000
                duty = 100*(elapsed % 1)
                t2ch1.pulse_width_percent(duty) # duty resets at 1, no pattern modification needed
                if advance_flag == 1:
                    print('Square Wave Pattern Selected')
                    state = 1 # loop back to FSM state 1 (square wave)
                    advance_flag = 0
                    time_s1_start = utime.ticks_ms()
                
            else:
                pass
                print('pass')
                # code to run if state number is invalid
                # program should ideally never reach here
        
        # Deinitialization by using/abusing ctrl-c interrupt 
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
