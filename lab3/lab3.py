'''@file        lab3.py
@brief          Calls class used to play "Simon Says" game
@details        Main program loop to call and execute simonTask.py. Additionally
                handles keyboard interrupts by end program and displaying
                win/loss ratio.
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab3/lab3.py,
                 video at https://photos.app.goo.gl/qxgqEeZGLetYLjXF7)
@author         Mark Iknoian. 
@date           2/18/21
@image          html lab3_fsm.png "Program FSM"
'''

import simonTask
import micropython

if __name__ == "__main__":  #only run as main program

    task1 = simonTask.simonTask(1,200000, DBG_flag=False)   # call simonTask 
    micropython.alloc_emergency_exception_buf(200) # memory for error handler

    while True:     # loop FSM calls until interrupt
        try:
            task1.run()
            
        except KeyboardInterrupt:   # on interrupt break loop and display wins/losses
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('See ya later! You had ' + str(task1.NumLosses) + ' losses and ' + str(task1.NumWins) + ' wins.')
            break

    

