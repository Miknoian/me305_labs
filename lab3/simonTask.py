'''@file        simonTask.py
@brief          Contains class for "Simon Says" game
@details        Holds class to run game and corresponding fucnctions.
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab3/simonTask.py,)
@author         Mark Iknoian. 
@date           2/18/21
'''

import utime
import pyb
import random

class simonTask:
    ''' @brief          Implements class for "Simon Says" style game
        @details        Class takes arguments for tasknum, update period, and debug
                        statements. Chooses a random 3 letter word and blinks LED in 
                        corresponding morse pattern, adding a letter each loop. 
                        User must then repeat pattern to win. 
    '''
    
    # Static variables are defined outside of methods
    
    ## State 1 constants
    S0_INIT          = 0
    S1_PLAY          = 1
    S2_LOSE          = 2
    S3_WIN           = 3
    
    # Morse translation code from
    # https://stackoverflow.com/questions/32094525/morse-code-to-english-python  
    global CODE                                             # morse translation table
    CODE = {'A': '.-',     'B': '-...',   'C': '-.-.', 
        'D': '-..',    'E': '.',      'F': '..-.',
        'G': '--.',    'H': '....',   'I': '..',
        'J': '.---',   'K': '-.-',    'L': '.-..',
        'M': '--',     'N': '-.',     'O': '---',
        'P': '.--.',   'Q': '--.-',   'R': '.-.',
        'S': '...',    'T': '-',      'U': '..-',
        'V': '...-',   'W': '.--',    'X': '-..-',
        'Y': '-.--',   'Z': '--..',

        '0': '-----',  '1': '.----',  '2': '..---',
        '3': '...--',  '4': '....-',  '5': '.....',
        '6': '-....',  '7': '--...',  '8': '---..',
        '9': '----.' 
        }
    
    global CODE_REVERSED
    CODE_REVERSED = {value:key for key,value in CODE.items()} # inverse of above table
    
    def __init__(self, taskNum, period, DBG_flag=True):
        ''' @brief          Constructs a simonTask.simonTask object.
            @details        This constructor allows task number, time period,
                            and debug flag to be specified during construction
            @param taskNum  Task number
            @param period   Time period to run new task every x uS
            @param DBG_flag Prints FSM related debug info
        '''
        
        ## Button input
        self.pinC13 = pyb.Pin (pyb.Pin.cpu.C13) # define variable for user button pin
        
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                    pull=pyb.Pin.PULL_NONE, callback=self.onButtonPressFCN) # When fall edge is detected on PC13, trigger callback fnc    

        ## LED output
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) # define variable for LED pin with push-pull output
        
        self.tim2 = pyb.Timer(2, freq = 20000) # Use timer 2 to create a clock frq of 20khz
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5) # Create channel 1 and set it to PWM mode for pin A5

        ## Button input state change flag
        self.ButtonTrigger = 0 # Initial state of advance flag
        
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
        ## Timestamp for last iteration of the task in microseconds
        self.lastTime = utime.ticks_us()
        
        ## The period for the task in microseconds
        self.period = period
        
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        ## An integer identifying the task
        self.taskNum = taskNum
        
        ## number of losses
        self.NumLosses = 0
        
        ## number of wins
        self.NumWins = 0
        
        ## possible words to choose from
        self.Words = ['leg', 'tie', 'tap', 'arm', 'inn', 'gap', 'pen', 'run', 'age', 'bow', 'bag', 'dip', 'nut', 'ash', 'ton', 'bar', 'hen', 'mud', 'far', 'car']
        
    
    def run(self):
    
        '''
        @brief      Handles FSM logic     
        '''
        ## current time stamp
        thisTime = utime.ticks_us()    
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            ## debug print statement for current state
            self.runs += 1
            if self.DBG_flag:
                print('T' + str(self.taskNum) + ': S' + str(self.state) + ": R" + str(self.runs))
            
            # main program code goes here
            if self.state==self.S0_INIT:
                # run state 0 (init) code
                print('Welcome! Press the button to begin.')
                self.transitionTo(self.S1_PLAY)   # Updating state for next iteration
                
            elif self.state==self.S1_PLAY:
                # run state 1 (play) code
                # If we are at the left, stop the motor and transition to S2
                # check for button input
                if self.ButtonTrigger:
                    # choose pseudo random word from self.Words
                    self.WordChoice = self.Words[random.randint(0,19)]
                    print(self.WordChoice.upper())
                    # Handoff word to game handler
                    self.MorseHandoff(str(self.to_morse(self.WordChoice)))
                
            elif self.state==self.S2_LOSE:
                # run state 2 (lose) code
                # if button is pressed, start next game
                if self.ButtonTrigger:
                    self.transitionTo(self.S1_PLAY)
                
            elif self.state==self.S3_WIN:
                # run state 3 (win) code
                # if button is pressed, start next game
                if self.ButtonTrigger:
                    self.transitionTo(self.S1_PLAY)
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        '''
        @brief               Handles transitions for FSM
        @param newState      Desired state to transition to
        '''
        # prints transition debug info if flag in main is 1
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState
         
        
    def onButtonPressFCN(self,IRQ_SRC):
        '''
        @brief      This function responds to a call back from button B1
        @param      The falling edge of a B1 button press triggers this call back function,
                    telling the FSM to advance to the next pattern
        '''

        # set false again with logic to track button fall/rise
        global ButtonTrigger
        self.ButtonTrigger = 1
 
        
    def MorseHandoff(self,HandoffInput):
        '''
        @brief                      Prepares morse input for LED output and later user input
        @param HandoffInput         Morse code to run game with
        '''
        # Morse input
        self.HandoffInput = HandoffInput
        # Split morse input into letters at space
        self.TextSplit = self.HandoffInput.split()
        # determine how many loops necessary (number of letters)
        self.HandoffLength = len(self.TextSplit)
        self.HandoffRecont = ''
        # global for stopping handoff function
        global HandoffStop
        self.HandoffStop = 0
        # global for checking win during final letter
        global HandoffFinalStop
        self.HandoffFinalStop = 0
        # loop iterates through each letter in word
        for self.HandoffPosition in range(0,self.HandoffLength):
            # lose condition
            if self.HandoffStop == 1:
                break
            # Sets up list to handoff to led/input functions
            elif self.HandoffPosition == 0:
                # list w/ first letter
                self.HandoffList = [self.TextSplit[0]]
                # add space after entry
                self.HandoffRecont =' '.join(self.HandoffList)
                # print(self.HandoffRecont)
                # call led/input functions
                self.MorseLoop(self.HandoffRecont)
                self.MorseInput(self.HandoffRecont)
            elif self.HandoffPosition >= 1:
                # for loop >1, list is appended with new morse characters and space is added
                self.HandoffList.append(self.TextSplit[self.HandoffPosition])
                self.HandoffRecont =' '.join(self.HandoffList)
                # print(self.HandoffRecont)
                self.MorseLoop(self.HandoffRecont)
                self.MorseInput(self.HandoffRecont)
                # print(self.HandoffPosition)
                # print(self.HandoffLength)
                # check for losing in final round
                if self.HandoffFinalStop == 1:
                    self.HandoffFinalStop = 0
                    break
                
                # win cnodition
                elif self.HandoffPosition + 1 == self.HandoffLength:
                    print('Congrats you Won! Press the button to play again, or ctrl-c to exit.')
                    # make pretty lights
                    self.MorseTiming(100000, 100000, 100)
                    self.MorseTiming(100000, 100000, 100)
                    self.MorseTiming(100000, 100000, 100)
                    self.MorseTiming(100000, 100000, 100)
                    self.MorseTiming(100000, 100000, 100)
                    # iterate wins by + 1
                    self.NumWins = self.NumWins + 1
                    # transition FSM to win state
                    self.transitionTo(self.S3_WIN)
                    
            
    def MorseLoop(self,LoopInput):
        '''
        @brief                  Iterates through morse letter by letter and calls LED function
        @param LoopInput        Morse code from handoff function
        '''
         
        # morse input
        self.LoopInput = LoopInput
        
        # check text length, set as loop length
        self.txt_length = len(self.LoopInput)
        self.MorseLastTime = utime.ticks_us()
        # print current letters for stage
        print(str(self.from_morse(self.LoopInput)))
        global LoopStop
        self.LoopStop = 0
        # for each letter, run configured LED timing function
        # Triggering LED for each morse character is configured in here
        for self.PositionNumber in range(0,self.txt_length):
            self.CurrentCharacter = self.LoopInput[self.PositionNumber]
            # print(str(self.PositionNumber) + ' for loop')
            # break loop on lose condition
            if self.LoopStop == 1:
                break
            elif self.CurrentCharacter == '-':
                self.timestamp = utime.ticks_us()
                # print('da')
                # print(str(self.timestamp))
                self.MorseTiming(1300000, 600000, 100)
                self.timestamp = utime.ticks_us()
                # print(str(self.timestamp))
            elif self.CurrentCharacter == '.':
                self.timestamp = utime.ticks_us()
                # print('dit') 
                # print(str(self.timestamp))
                self.MorseTiming(350000, 600000, 100)
                self.timestamp = utime.ticks_us()
                # print(str(self.timestamp))
            elif self.CurrentCharacter == ' ':  
                self.timestamp = utime.ticks_us()                
                # print('space')
                # print(str(self.timestamp))
                self.MorseTiming(700000, 0, 0)
                self.timestamp = utime.ticks_us()
                # print(str(self.timestamp))
                
    def MorseTiming(self, Length, CharacterPause, PWMpercent):
        '''
        @brief                  Controls LED timing/brightness for specified input
        @param Length           Length of time on in uS
        @param CharacterPause   Length of time off before turning on
        @param PWMpercent       LED brightness percentage
        '''
        # length of time on
        self.Length = Length
        
        # length of time off before turning on
        self.CharacterPause = CharacterPause
        
        # current time stamp
        self.MorseLastTime = utime.ticks_us()
        
        # time to pause until
        self.MorsePauseTime = utime.ticks_add(self.MorseLastTime, self.CharacterPause)
        
        # time to turn on at
        self.MorseNextTime = utime.ticks_add(self.Length, self.MorsePauseTime)
        self.loop_true = 1
        self.PWMpercent = PWMpercent
        # wait pause time, turn on LED @ self.PWMpercent for self.Length time
        while self.loop_true == 1:
            self.MorseThisTime = utime.ticks_us()
            if utime.ticks_diff(self.MorseThisTime, self.MorsePauseTime) >= 0:
                self.t2ch1.pulse_width_percent(PWMpercent)                     
                if utime.ticks_diff(self.MorseThisTime, self.MorseNextTime) >= 0:
                    self.t2ch1.pulse_width_percent(0)                  
                    self.loop_true = 0 
            
    def MorseInput(self, LoopInput):
        '''
        @brief                  Iterates through morse letter by letter and calls input function
        @param LoopInput        Morse code from handoff function
        '''
        # identical to MorseLoop except for space character handling and function called for input
        self.LoopInput = LoopInput
        self.txt_lengthInput = len(self.LoopInput)
        self.MorseLastTime = utime.ticks_us()
        global LoopStopInput
        self.LoopStopInput = 0
        for self.PositionNumberInput in range(0,self.txt_lengthInput):
            self.CurrentCharacter = self.LoopInput[self.PositionNumberInput]
            # print(str(self.PositionNumber) + ' for loop')
            if self.LoopStopInput == 1:
                break
            elif self.CurrentCharacter == '-':
                self.timestamp = utime.ticks_us()
                # print('da INPUT')
                # print(str(self.PositionNumberInput) + 'pos')
                # print(str(self.timestamp))
                self.MorseInputTiming(1300000, 900000)
                self.timestamp = utime.ticks_us()
                # print(str(self.timestamp))
            elif self.CurrentCharacter == '.':
                self.timestamp = utime.ticks_us()
                # print('dit INPUT') 
                # print(str(self.timestamp))
                self.MorseInputTiming(350000, 350000)
                self.timestamp = utime.ticks_us()
                # print(str(self.timestamp))
            
            # basically wait x uS for space character
            elif self.CurrentCharacter == ' ':  
                self.timestamp = utime.ticks_us()                
                # print('space INPUT')
                self.SpaceLoop = 1
                while self.SpaceLoop == 1:
                    self.CurrentTime = utime.ticks_us()
                    if utime.ticks_diff(self.CurrentTime, self.timestamp) >= 700000:
                        self.SpaceLoop = 0
                        pass

    def MorseInputTiming(self, Length, Tolerance):
        '''
        @brief              Controls input timing/corretness for specified input
        @param Length       Length of on time in uS to win
        @param Tolerance    +/- tolerance for off time inputs
        '''
        # confirm button is 0 to avoid edge cases
        self.ButtonTrigger = 0
        
        # input time window
        self.Length = Length
        
        # +/- tolerance for time window
        self.Tolerance = Tolerance
        
        # construct time bounds
        self.TimingBoundLower = self.Length - self.Tolerance
        self.TimingBoundUpper = self.Length + self.Tolerance
        self.MorseLastTime = utime.ticks_us()
        
        # inactive time to trigger lose state
        self.MorseTimeOut = 5000000
        
        # time stamp for loss
        self.MorseTimeOutCount = utime.ticks_add(self.MorseTimeOut, self.MorseLastTime)
        self.loop_true = 1
        while self.loop_true == 1:
            self.MorseThisTime = utime.ticks_us()
            
            # inactive lose state check
            if utime.ticks_diff(self.MorseThisTime, self.MorseTimeOutCount) >= 0:
                print('You lost. Press the button to play again or ctrl-c to exit.')
                
                # iterate losses
                self.NumLosses = self.NumLosses + 1
                
                # flash pretty colors
                self.MorseTiming(500000, 500000, 100)
                self.MorseTiming(500000, 500000, 100)
                self.MorseTiming(500000, 500000, 100)
                self.ButtonTrigger = 0
                
                # transition FSM
                self.transitionTo(self.S2_LOSE)
                
                # stop led activation loop (MorseTiming)
                self.loop_true = 0
                
                # stop led calling/timing loop (MorseLoop)
                self.LoopStop = 1
                
                # stop input loop (MorseInput) 
                self.LoopStopInput = 1
                
                # stop handoff loop except win check for edge case (MorseHandoff)
                self.HandoffStop = 1
                
                # stop handoff loop win check for edge case (MorseHandoff)
                self.HandoffFinalStop = 1
                
                # back to FSM
                self.run()
                
            # when button is pressed, flash LED and wait for rise
            elif self.ButtonTrigger == 1:
                self.t2ch1.pulse_width_percent(100) 
                self.ButtonTrigger = 0
                self.ButtonLoop = 1
                self.MorseInputStart = utime.ticks_us()
                
                # on rise, compare time depressed to specified time bounds
                while self.ButtonLoop == 1:
                    self.MorseFallTime = utime.ticks_us()
                    self.MorseStallTime = utime.ticks_diff(self.MorseFallTime, self.MorseInputStart)
                    if self.ButtonTrigger == 1:
                        self.t2ch1.pulse_width_percent(0) 
                        self.MorseInputEnd = utime.ticks_us()
                        self.MorseInputDif = utime.ticks_diff(self.MorseInputEnd, self.MorseInputStart)
                        
                        # if within bounds, letter is correct
                        if self.TimingBoundLower < self.MorseInputDif < self.TimingBoundUpper:
                            self.ButtonTrigger = 0
                            self.loop_true = 0
                            self.ButtonLoop = 0
                            # print('win')
                        
                        # if not within bounds, letter is wrong
                        else:
                            print('You lost. Press the button to play again or ctrl-c to exit.')
                            
                            # see above loss for comments
                            self.NumLosses = self.NumLosses + 1
                            self.MorseTiming(500000, 500000, 100)
                            self.MorseTiming(500000, 500000, 100)
                            self.MorseTiming(500000, 500000, 100)
                            self.ButtonTrigger = 0
                            self.transitionTo(self.S2_LOSE)
                            self.loop_true = 0
                            self.ButtonLoop = 0 
                            self.LoopStop = 1
                            self.LoopStopInput = 1
                            self.HandoffStop = 1
                            self.HandoffFinalStop = 1
                    
                    # time out condition whike button is depressed
                    elif utime.ticks_diff(self.MorseStallTime, self.MorseTimeOut) >= 0:
                        print('You lost. Press the button to play again or ctrl-c to exit.')
                        
                        # see above loss for comments
                        self.NumLosses = self.NumLosses + 1
                        self.MorseTiming(500000, 500000, 100)
                        self.MorseTiming(500000, 500000, 100)
                        self.MorseTiming(500000, 500000, 100)
                        self.ButtonTrigger = 0
                        self.transitionTo(self.S2_LOSE)
                        self.loop_true = 0
                        self.ButtonLoop = 0
                        self.LoopStop = 1
                        self.LoopStopInput = 1
                        self.HandoffStop = 1
                        self.HandoffFinalStop = 1
    
    # Morse translation code from
    # https://stackoverflow.com/questions/32094525/morse-code-to-english-python                    
    def to_morse(self, Input):
        '''
        @brief              Translate input to Morse code
        @param Input        text input
        '''
        self.Input = Input        
        return ' '.join(CODE.get(i.upper()) for i in self.Input)

    def from_morse(self, Input):
        '''
        @brief              Translate Morse code to text
        @param Input        Morse input
        '''
        self.Input = Input
        return ''.join(CODE_REVERSED.get(i) for i in self.Input.split())




                    







        
        
        
        
        
                
            
        
        

        

