'''@file        encoderTask.py
@brief          Contains class for encoder drivers
@details        Holds class to create drivers that can send/set encoder position
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab0xFFwk3/encoderTask.py)
@author         Mark Iknoian. 
@date           3/18/21
'''

import utime
import pyb

class encoderTask:
    ''' @brief          Implements class for encoder drivers
        @details        Class looks at encoder input to generate an absolute position.
                        Considers reading validity for overflow. Can also set position.
    '''

    def __init__(self, timerNum, prescaler, period, pin1, chnl1 , pin2, chnl2):
        ''' @brief          Constructs a encoderTask.encoderTask object.
            @details        This constructor takes frequency parameters, encoder
                            pins/channels and creates a HW driver
            @param timerNum     number of timer to use
            @param prescaler    specifies what counts as a "Tick"
            @param period       value encoder will roll over at (<16 bit)
            @param pin1         specified input pin1
            @param chl1         specified channel in timer
            @param pin2         specified input pin2
            @param chl2         specified channel in timer
        '''
        
        self.timerNum = timerNum
        
        self.prescaler = prescaler
        
        self.period = period
        
        self.pin1 = pin1

        self.chnl1 = chnl1
                
        self.pin2 = pin2
        
        self.chnl2 = chnl2
        
        ## create timer object
        self.timer = pyb.Timer(self.timerNum)
        
        ## define timer
        self.timer.init(prescaler = self.prescaler, period = self.period)
        
        
        # create channel in timer object for encoders
        self.timer.channel(self.chnl1, pin=self.pin1, mode=pyb.Timer.ENC_AB)
        self.timer.channel(self.chnl2, pin=self.pin2, mode=pyb.Timer.ENC_AB)
        
        
        self.delta = 0
        
        self.currentPos = 0
        
        self.runningPos = 0
  
    def update(self):
        '''
        @brief               Updates encoder position for object
        '''
        self.delta = self.timer.counter() - self.currentPos
        self.currentPos = self.timer.counter()
        if self.delta > self.period/2:
            self.runningPos += self.delta - self.period
        elif self.delta < -self.period/2:
            self.runningPos += self.delta + self.period
        else:
            self.runningPos += self.delta
        
        
    def get_position(self):
        '''
        @brief              Returns current encoder position
        @return             Current encoder positon
        '''
        return int(self.runningPos)
        
    def set_position(self,setPos):
        '''
        @brief              Set current encoder position
        '''
        self.runningPos = setPos
        
    def get_delta(self):
        '''
        @brief              Returns previous tick delta
        @return             previous tick delta
        '''
        return self.delta
        
        

    


