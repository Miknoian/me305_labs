'''@file        ClosedLoop.py
@brief          Contains class for closed loop gain controller
@details        Holds class to create controller and set gain.
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab0xFFwk3/ClosedLoop.py)
@author         Mark Iknoian. 
@date          3/18/21
'''

class ClosedLoop:
    ''' @brief          Implements class for closed loop controller
        @details        Class takes arguments for gain when constructed. Error 
                        multiplied by controller gain can be returned, updated,
                        and modified.
    '''    
    
    def __init__(self, gain):
        '''
        @brief                      Constructs a ClosedLoop.ClosedLoop object.
        @param gain                 Specified controller gain
        '''
        # divide gain by voltage high
        self.gain = gain/3.6
        
    def update(self,omega_ref, omega_act):
        '''
        @brief                      Updates/runs error value calculation
        @param omega_ref            desired motor speed in RPM
        @param omega_ref            actual motor speed in RPM
        @return                     New duty %
        ''' 
        # basic P controller eq
        return self.gain*(omega_ref-omega_act)
        
    def get_Kp(self):
        '''
        @brief                      Returns current gain value
        @return                     current controller gain
        ''' 
        return self.gain
        
    def set_Kp(self, newGain):
        '''
        @brief                      Sets new controller gain
        @param newGain              Chosen new gain for controller
        ''' 
        self.newGain = newGain
        
        self.gain = self.newGain/3.6
        
        
