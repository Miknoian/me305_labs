'''@file        motorTask.py
@brief          Contains class to create motor driver
@details        Contructs motor driver that can manage duty cycles through
                PWM percentage.
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab0xFFwk3/motorTask.py)
@author         Mark Iknoian. 
@date           3/18/21
'''

import pyb

class motorTask:
    ''' @brief          Implements class for motor driver
    '''

    def __init__(self, nSLEEP_pin, IN1_pin, chnl1, IN2_pin, chnl2, mottim):
        ''' @brief          Constructs a motorTask.motorTask object
            @details        Takes specific inputs and contructs motor object that 
                            takes duty cycle % and actuates HW
            @param nSLEEP_pin   board sleep pin
            @param IN1_pin      motor input pin 1
            @param chnl1        timer channel 1 for motor
            @param IN2_pin      motor input pin 2
            @param chnl2        timer channel 2 for motor
            @param mottim       specified motor timer
        '''
        
        self.nSLEEP_pin = pyb.Pin(nSLEEP_pin)
        
        
        # set sleep pin mode
        self.nSLEEP_pinRun = pyb.Pin(self.nSLEEP_pin, pyb.Pin.OUT_PP)
        
        self.IN1_pin = pyb.Pin(IN1_pin)
        
        self.IN2_pin = pyb.Pin(IN2_pin)
        
        self.nSLEEP_pinRun.low()
        
        self.chnl1 = chnl1
        
        self.chnl2 = chnl2
        
        #set channels for timer
        self.tch1 = mottim.channel(self.chnl1, pin = self.IN1_pin, mode = pyb.Timer.PWM)
        self.tch2 = mottim.channel(self.chnl2, pin = self.IN2_pin, mode = pyb.Timer.PWM)
        
        print ('Creating a motor driver')
        # print(self.tim)
    
    def enable(self):
        
        '''
        @brief      enable motor by setting sleep pin high    
        '''
        self.nSLEEP_pinRun.high()
        print ('Enabling Motor')
    
    def disable(self):
        '''
        @brief      disables motor by setting sleep pin low    
        '''
        self.nSLEEP_pinRun.low()
        print ('Disabling Motor')
    
    def set_duty(self, duty):
        '''
        @brief      sets motor duty cycle
        @param duty     specifies motor duty
        '''
        self.duty = duty
        if self.duty > 0:
            if self.duty > 100:
                self.tch2.pulse_width_percent(0)                
                self.tch1.pulse_width_percent(100)
            else:
                self.tch2.pulse_width_percent(0)
                self.tch1.pulse_width_percent(self.duty)
        
        # check if reverse dirrection requested
        else:    
            if self.duty < -100:
                self.tch1.pulse_width_percent(0)
                self.tch2.pulse_width_percent(100)
            else:
                self.tch1.pulse_width_percent(0)                
                self.tch2.pulse_width_percent(abs(self.duty))
    
    def get_duty(self):
        return self.duty
        '''
        @brief      returns motor duty cycle   
        @return     current motor duty cycle
        '''
        
