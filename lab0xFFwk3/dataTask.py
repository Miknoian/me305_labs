'''@file        dataTask.py
@brief          Contains class for data collection FSM
@details        Holds class to create FSM and conumicate with UI over serial
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab0xFFwk3/dataTask.py)
@author         Mark Iknoian. 
@date           3/18/21
'''

import utime
import pyb
from pyb import UART
from array import array
from math import exp, sin, pi
pyb.repl_uart(None)

class dataTask:
    ''' @brief          Implements class data collection for week 2
        @details        Class accepts serial input to collect and return data.
                        For more robust/commented code look at controlTask.controlTask ,
                        which is an iterated version of this code base.
    '''
    
    # Static variables are defined outside of methods
    
    ## State 1 constants
    S0_INIT          = 0
    S1_WAIT          = 1
    S2_COLLECT       = 2
    S3_PRINT         = 3

    
    def __init__(self, taskNum, period, DBG_flag=True):
        ''' @brief          Constructs a dataTask.dataTask object
        '''
        
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
        ## Timestamp for last iteration of the task in microseconds
        self.lastTime = utime.ticks_us()
        
        ## The period for the task in microseconds
        self.period = period
        
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        ## An integer identifying the task
        self.taskNum = taskNum
        
        self.myuart = UART(2)
        
    print('constructor')    
    
    def run(self):
        print('run')
    
        '''
        @brief      Handles FSM logic     
        '''
        ## current time stamp
        self.thisTime = utime.ticks_us()   
        print(utime.ticks_diff(self.thisTime, self.nextTime))
        if utime.ticks_diff(self.thisTime, self.nextTime) >= 0:
            print('time check')
        # if True:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            ## debug print statement for current state
            self.runs += 1
            if self.DBG_flag:
                print('T' + str(self.taskNum) + ': S' + str(self.state) + ": R" + str(self.runs))
            
            # main program code goes here
            if self.state==self.S0_INIT:
                print('s0')
                self.state = self.S1_WAIT
                
            elif self.state==self.S1_WAIT:
                print('s1')
                if self.myuart.any() != 0:
                    print('char check')
                    self.val = self.myuart.readchar()
                    print(self.val)
                    if self.val == 71:
                        self.values = array('f', 600*[0])
                        self.time = array('f', 600*[0])
                        self.state = self.S2_COLLECT 
                    
            elif self.state==self.S2_COLLECT:
                print('s2')
                self.timestampRef = utime.ticks_us()
                self.recordInt = 50000
                self.record = 1
                self.timestampNext = utime.ticks_add(self.timestampRef,self.recordInt)
                self.collectPos = 0
                while self.record == 1:
                    self.timestampNow = utime.ticks_us()
                    print(utime.ticks_diff(self.timestampNow,self.timestampNext))
                    if utime.ticks_diff(self.timestampNow,self.timestampNext) >= 0:
                        print(self.collectPos)
                        self.timestampNext = utime.ticks_add(self.timestampNow,self.recordInt)
                        self.timestampWrite = utime.ticks_diff(self.timestampNow,self.timestampRef)
                        self.time[self.collectPos] = self.timestampWrite/1000000
                        self.values[self.collectPos] = exp((-1/10)*self.timestampWrite/1000000)*sin(2*pi/3*self.timestampWrite/1000000)
                        self.collectPos = self.collectPos + 1
                        if self.collectPos == 600:
                            self.collectPos = 0
                            self.record = 0
                            self.state = self.S3_PRINT 
                        
                self.state = self.S3_PRINT 
                
            elif self.state==self.S3_PRINT:
                print('s3')
                self.myuart.write('{:},{:}\r\n'.format(self.time,self.values)) 
                self.state = self.S1_WAIT 
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        '''
        @brief               Handles transitions for FSM
        @param newState      Desired state to transition to
        '''
        # prints transition debug info if flag in main is 1
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState

