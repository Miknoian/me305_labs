'''@file        lab0xFF_nucleo_main.py
@brief          Contains class to initiate servo construction on Nucleo
@details        Contructs required timer, controller, motor, and encoder objects to
                be handed off to other classes. Runs main loop for controlTask 
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab0xFFwk3/lab0xFF_nucleo_main.py)
@author         Mark Iknoian. 
@date           3/18/21
'''

import micropython
from pyb import UART
import pyb
import encoderTask
import motorTask
import ClosedLoop
import controlTask
pyb.repl_uart(None)

if __name__ == "__main__":  #only run as main program

    myuart = UART(2)
    encoder1 = encoderTask.encoderTask(4 , 0, 65535, pyb.Pin.cpu.B6, 1, pyb.Pin.cpu.B7, 2)
    # encoder2 = encoderTask.encoderTask(8 , 0, 65535, pyb.Pin.cpu.C6, 1, pyb.Pin.cpu.C7, 2)
    
    mottim = pyb.Timer(3, freq = 20000)
    
    motor1 = motorTask.motorTask(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, 1, pyb.Pin.cpu.B5, 2, mottim)
    # motor2 = motorTask.motorTask(pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, 3, pyb.Pin.cpu.B1, 4, mottim)
    micropython.alloc_emergency_exception_buf(200) # memory for error handler
    
    controller1 = ClosedLoop.ClosedLoop(1)
    ctrl1 = controlTask.controlTask(1,50000,500,motor1,encoder1,controller1,2000,5)
    
    while True:
        try:
            ctrl1.run()

        except KeyboardInterrupt:   # on interrupt break loop and display wins/losses
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('See ya later!')
            break


