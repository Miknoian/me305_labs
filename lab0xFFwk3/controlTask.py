'''@file        controlTask.py
@brief          Contains class for control FSM
@details        Holds class to create FSM that manages controller,motor, and 
                encoders
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab0xFFwk3/controlTask.py)
@author         Mark Iknoian. 
@date           3/18/21
'''

import utime
import pyb
from pyb import UART
from array import array
pyb.repl_uart(None)

class controlTask:
    ''' @brief          Implements tasks system for handling servo logic
        @details        Class interfaces with specified encoder, motor, and controller
                        objects to create servo system at specified speed. Data
                        is recored for specified length and exported through serial
                        to the User Interface to be plotted.
    '''
    
    # Static variables are defined outside of methods
    
    ## State 1 constants
    S0_INIT          = 0
    S1_WAIT          = 1
    S2_COLLECT       = 2
    S3_PRINT         = 3

    
    def __init__(self, taskNum, period, ctrlPeriod, motor, encoder, controller, desiredOmega, gain, DBG_flag=False):
        ''' @brief          Constructs a controlTask.controlTask object
            @details        Takes existing controller, motor, and encoder object
                            to create control task.
            @param taskNum      task number
            @param period       period to collect data at
            @param ctrlPeriod   period to run controller
            @param motor        reference to existing motor class
            @param encoder      reference to existing encoder class
            @param controller   reference to existing controller class
            @param desiredOmega     desired servo speed in RPM
            @param gain         desired controller gain for controller class
        '''
        
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
        ## Timestamp for last iteration of the task in microseconds
        self.lastTime = utime.ticks_us()
        
        ## The period for the task in microseconds
        self.period = period
        
        self.ctrlPeriod = ctrlPeriod
        
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        ## An integer identifying the task
        self.taskNum = taskNum
        
        ## UART pin setup for "2"
        self.myuart = UART(2)
        
        self.motor = motor
        
        self.encoder = encoder
        
        self.controller = controller
        
        self.gain = gain
        
        self.currentOmega = 0
        
        self.desiredOmega = desiredOmega
        
        ## set gain in provided controller class
        self.controller.set_Kp(self.gain)
        
        ## take motor out of sleep mode
        self.motor.enable()
        
    print('constructor')    
    
    def run(self):
        '''
        @brief      Handles FSM logic     
        '''
        ## current time stamp
        self.thisTime = utime.ticks_us()   
        if utime.ticks_diff(self.thisTime, self.nextTime) >= 0:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            ## debug print statement for current state
            self.runs += 1
            if self.DBG_flag:
                print('T' + str(self.taskNum) + ': S' + str(self.state) + ": R" + str(self.runs))
            
            # main program code goes here
            if self.state==self.S0_INIT:
                self.state = self.S1_WAIT
             
            # wait for "G" input over serial and create array/switch states
            elif self.state==self.S1_WAIT:
                if self.myuart.any() != 0:
                    print('char check')
                    self.val = self.myuart.readchar()
                    if self.val == 71:
                        self.values = array('f', 600*[0])
                        self.time = array('f', 600*[0])
                        self.state = self.S2_COLLECT 
            
            # state to run servo/collect data
            elif self.state==self.S2_COLLECT:
                # initial time stamp
                self.timestampRef = utime.ticks_us()
                # interval to record data
                self.recordInt = 500
                # iterated on for while exit condition
                self.record = 1
                
                # timestamps to record data/update controller
                self.timestampNext = utime.ticks_add(self.timestampRef,self.recordInt)
                self.timestampNextCtrl = utime.ticks_add(self.timestampRef,self.ctrlPeriod)
                self.collectPos = 0
                while self.record == 1:
                    self.timestampNow = utime.ticks_us()
                    # data collection
                    if utime.ticks_diff(self.timestampNow,self.timestampNext) >= 0:
                        # specify next timestamp to collect
                        self.timestampNext = utime.ticks_add(self.timestampNow,self.recordInt)
                        self.timestampWrite = utime.ticks_diff(self.timestampNow,self.timestampRef)
                        # record time (starts @ 0)
                        self.time[self.collectPos] = self.timestampWrite/1000000
                        # get encoder update
                        self.encoder.update()
                        #record speed in RPM
                        self.values[self.collectPos] = (self.encoder.get_delta()/4000)*(60)*(1000000/self.ctrlPeriod)
                        self.collectPos = self.collectPos + 1
                        if self.collectPos == 600:
                            self.collectPos = 0
                            self.record = 0
                            self.state = self.S3_PRINT
                    
                    # controller update
                    if utime.ticks_diff(self.timestampNow,self.timestampNextCtrl) >= 0:
                        self.encoder.update()
                        self.currentOmega = (self.encoder.get_delta()/4000)*(60)*(1000000/self.ctrlPeriod)
                        # call controller to get new duty cycle based on omegas
                        self.new_duty = self.controller.update(self.desiredOmega,self.currentOmega)
                        self.motor.set_duty(self.new_duty)
                self.state = self.S3_PRINT 
            
            # send data over serial
            elif self.state==self.S3_PRINT:
                self.motor.set_duty(0)
                self.myuart.write('{:},{:}\r\n'.format(self.time,self.values)) 
                self.state = self.S1_WAIT 
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        '''
        @brief               Handles transitions for FSM
        @param newState      Desired state to transition to
        '''
        # prints transition debug info if flag in main is 1
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState

