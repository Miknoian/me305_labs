'''@file        lab0xFF.py
@brief          User Interface for communicating with Nucleo driven servo
@details        Sends start message for servo and recieves collected velocity data
                during run. Plots in Python.
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab0xFFwk3/lab0xFF.py)
@author         Mark Iknoian. 
@date           3/18/21
@image          html wk1_graph.png "Graph of data collected from Nucleo for week 1"
@image          html 305_final.png "Controller Task FSM"
@image          html kp.1.png "Step response at low Kp of .1. Note large settling time and inability to reach desired steady state"
@image          html kp.5.png "Step response at Kp of .5. Note large settling time but achieves desired steady state value"
@image          html kp1.png "Step response at Kp of 1. Quickly reaches desired steady state value without saturation"
@image          html kp5.png "Step response at Kp of 5. Quickly reaches desired steady state value with saturation. No difference from Kp = 1"
'''

import serial
from matplotlib import pyplot
from array import array
import numpy

# setup serial communication
ser = serial.Serial(port='COM7',baudrate=115273,timeout=40)

# setup blank arrays for data
values = array('f', 600*[0])
time = array('f', 600*[0])


while True:
    
    # take user input
    inv = input('Give me a character: ')
    
    # "G" initiates data collection + servo activation on Nucleo
    if inv == 'G':
        ser.write(str(inv).encode('ascii'))
        # wait for serial coms and format recieved data to place in array
        myval = ser.readline().decode('ascii')
        print('read')
        myStrippedVal = myval.translate({ord(i): None for i in "ary[]'()f"})
        myFixedVal = myStrippedVal.strip(', ')
        mySplitVal = myFixedVal.split(',')
        count = 0
        
        # place data in arrays
        for count in range(0,600):
            time[count] = float(mySplitVal[count])
            count =+ 1
        count = 601
        for count in range(601,1201):
            values[count-601] = float(mySplitVal[count])
            count =+ 1
        print(time)
        print(values)
        
        # save & plot recieved data
        numpy.savetxt("out.csv", (time,values), delimiter=',')
        pyplot.figure()
        pyplot.plot(time, values)
        pyplot.xlabel('Time')
        pyplot.ylabel('RPM')
        pyplot.axis([0,.25,0,4000])
        pyplot.show()
        





