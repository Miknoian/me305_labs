import time
import random

# function definitions
def motor_cmd(cmd):
    if cmd=='FWD':
        print('Mot forawrd')
    elif cmd=='REV':
        print('Mot reverse')
    elif cmd=='STOP':
        print('Mot stop')

def first():
    return random.choice([True, False]) # randomly returns T/F

def second():
    return random.choice([True, False]) # randomly returns T/F

def button_1():
    return random.choice([True, False]) # randomly returns T/F

def button_2():
    return random.choice([True, False]) # randomly returns T/F



# Main program begin
if __name__ == "__main__":
    # Program initialization
    state = 0 # initial state
    
    while True:
        try:
            # main program code here
            if state==0:
                # run state 0 code
                print('s0')
                motor_cmd('FWD') # command motor to go forward
                state = 1 # updating state for next iteration
                
            elif state==1:
                pass
                # run state 1 (moving left) code
                print('s1')
                # If we are at left stop motor and transistion to s2
                if L_sensor():
                    motor_cmd('STOP')
                    state=2
                
            elif state==2:
                pass
                # run state 2 (stopped at left) code
                print('s2')
                if go_button():
                    motor_cmd('REV')
                    state=3
                
            elif state==3:
                pass
                # run state 3 (moving right) code
                print('s3')
                if R_sensor():
                    motor_cmd('STOP')
                    state=4
                
            elif state==4:
                pass
                # run state 4 (stopped at right) code
                print('s4')
                motor_cmd('FWD')
                state = 1
                
            else:
                pass
                # code to run if state number is invalid
                # program should not reach here
           
            # slow execution for observation
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            #This except block catches "ctrl-c" to end while (True) loop
            break 

    # program de-initialization goes here
    print('After')