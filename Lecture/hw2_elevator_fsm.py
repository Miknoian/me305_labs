'''@file        hw2_elevator_fsm.py
@brief          Simulates a two floor elevator
@details        Implements a finite state machine, shown below, to simulate
                the behavior of a two floor elevator
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/Lecture/hw2_elevator_fsm.py)
@author         Original code by Charlie Refvem. Modified by Mark Iknoian. 
@date           1/27/21
'''

import time
import random


def motor(cmd):
    '''@brief Commands the motor to move or stop
       @param cmd The command to give the motor
    '''
    if cmd=='1':
        print('Elevator Moving Up')
    elif cmd=='2':
        print('Elevator Moving Down')
    elif cmd=='0':
        print('Elevator Stopped on Floor {:}'.format(floor_num))
        # Floor_num is updated accordingly for each floor

# Unfortunately a full elevator and control system was unable to be obtained
# for this assignment. Instead, the "buttons"/"sensors" are randomly triggered

def floor_1():
    '''@brief Randomly returns T/F to simulate floor 1 sensor
    '''
    return random.choice([True, False]) # randomly returns T or F

def floor_2():
    '''@brief Randomly returns T/F to simulate floor 2 sensor
    '''
    return random.choice([True, False]) # randomly returns T or F

def button_1():
    '''@brief Randomly returns T/F to simulate button 1
    '''
    return random.choice([True, False]) # randomly returns T or F

def button_2():
    '''@brief Randomly returns T/F to simulate button 2
    '''
    return random.choice([True, False]) # randomly returns T or F

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state
    first = 1 # Initial state for first pass check
    
    while True:
        try:
            # main program loop for FSM
            if state==0 and first==1:
                # First pass state when program is initialized. This is done 
                # to keep consistency with console output/motor cmds in other 
                # states. Without this check, the motor command would be sent 
                # every time the loop is ran while in this state.
                print('S0')
                motor('2')
                # Set motor state to 'moving down'
                first=0
                # Clear variable and move onto modified state 0 for subsequent
                # loops
                    
            if state==0 and first==0:
                # run state 0 general code
                print('S0')
                # if we reach floor 1, stop motor and transisiton to S1
                if floor_1():
                    floor_num = '1'
                    # Set variable value to be called when motor is stopped
                    # To indicate floor in print() command
                    motor('0')
                    state = 1   
                    # Updating state for next iteration                    
        
            elif state==1:
                # run state 1 (Stopped on Floor 1) code
                print('S1')
                # if the floor 2 button is pressed, start motor moving upwards
                # and transition to S2
                if button_2():
                    motor('1')
                    state=2
                
            elif state==2:
                # run state 2 (Moving Up) code
                print('S2')
                # if we reach floor 2, stop motor and transisiton to S3
                if floor_2():
                    floor_num = '2'
                    motor('0')
                    state=3
                
            elif state==3:
                # run state 3 (Stopped on FLoor 2) code
                print('S3')
                # if the floor 1 button is pressed, start motor moving down
                # and transition to S0
                if button_1():
                    motor('2')
                    state=0
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
            
            # Slow down execution of FSM so we can see output in console
            time.sleep(0.2)
        
        # Deinitialization by using/abusing ctrl-c interrupt 
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
