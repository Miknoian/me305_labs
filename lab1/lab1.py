'''@file        lab1.py
@brief          Calculates Fibonacci number from provided index
@details        Checks user input and uses an iterative list to return a 
                Fibonacci number at a requested index 
                (Hosted at https://bitbucket.org/Miknoian/me305_labs/src/master/lab1/lab1.py)
@author         Mark Iknoian
@date           1/20/21
'''

def fib (idx):
    '''
    @brief      This function calculates a Fibonacci number at a specific index.
    @param      idx An integer specifying the index of the desired
                Fibonacci number
    @return     Calculated Fibonacci number at specified index
    '''
    #   Start list to be appended
    fiblist = [0,1,1]
    #   Specify first three indexs, as pattern must be established
    if idx == 0:
        return fiblist[0]
    elif idx == 1:
        return fiblist[1]
    elif idx == 2:
        return fiblist[2]
    #   If greater than first three, begin pattern computation
    else:
        n = 2
        #   Loop to append the sum of last two values until index is reached
        while n < idx:
            fiblist.append(fiblist[n] + fiblist[n-1])
            n = n+1
        #   After patern has been calculated to necessary value, fib() returns
        #   the last value of the list
        return fiblist[n]    
    return 0

#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == '__main__':
    #   Set variable that never changes to keep the while loop running after data entry
    dummy = 0
    while dummy == 0:    
        #   Prompts user with message using the input command
        #   User input replaces contents of variable user_in
        #   Initate check (T/F) to see if input is a valid integer
        user_in = input('Please enter a Fibonacci index: ')
        check = user_in.isdigit()
        #   User input is valid. Convert string input to integer
        #   Hand off to fib() by writing to variable idx
        if check == 1:
            idx = int(user_in)
            #   Print string with variable/called function information
            print ('Fibonacci number at index {:} is {:}.'.format(idx,fib(idx)))
        else:
            #   User input is not valid
            #   Reply with slightly sarcastic string in console
            print ('Thats a funny looking integer')
            #   Always returns to the console message bellow by calling input
            #   cont returns to beginning of while loop
    cont = input('Please enter a Fibonacci index number')
  